const router = require('express')();
const {default: Resolution} = require('@unstoppabledomains/resolution');
const resolution = new Resolution();

router.get('/:domain',async (req,res)=>{
    // res.json({'domain':resolveHash(req.params.domain)});
    const data = await resolvedHash(req.params.domain);
    // resolveIpfsHash('iloveopera.crypto');
    res.json({'address':data});
});

async function resolvedHash(domain) {
    try{
        const resolvedAddress = await resolution.ipfsHash(domain);
        return resolvedAddress;
    }catch(e){
        return e.code;
    }
    return 'N/A';
  }

  function resolveIpfsHash(domain) {
    resolution
      .ipfsHash(domain)
      .then((hash) =>
        console.log(
          `You can access this website via a public IPFS gateway: https://gateway.ipfs.io/ipfs/${hash}`,
        ),
      )
      .catch(console.error);
  } 


module.exports = router;